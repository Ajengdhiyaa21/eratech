<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Alat Camping - Login</title>

    <!-- Custom fonts for this template-->
    <link href="admin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Serif&family=Poppins:ital,wght@0,100;0,300;0,400;0,700;1,700&display=swap" rel="stylesheet" />
    <script src="https://unpkg.com/feather-icons"></script>

</head>

<body>
  <!-- Navbar -->
  <div class="container">
    <nav class="navbar fixed-top bg-body-secondary navbar-expand-lg">
      <div class="container">
        <!-- <a class="navbar-brand" href="#">
          <img src="admin/img/logo.jpeg" alt="Logo" class="d-inline-block align-text-top">
        </a> -->
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mx-auto mb-2 mb-lg-0">
            <li class="nav-item ">
              <a class="nav-link active" aria-current="page" href="#home">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="#about">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="#bayar">Tata Cara</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="#contact">Kontak</a>
            </li>
          </ul>
          <?php
          if (isset($_SESSION['id_user'])) {
            // jika user telah login, tampilkan tombol profil dan sembunyikan tombol login
            echo '<a href="profil.php" data-bs-toggle="modal" data-bs-target="#profilModal" class="btn btn-inti"><i data-feather="user"></i></a>';
          } else {
            // jika user belum login, tampilkan tombol login dan sembunyikan tombol profil
            echo '<a href="login.php" class="btn btn-inti" type="submit">Login</a>';
          }
          ?>

        </div>
      </div>
    </nav>
  </div>
  <!-- End Navbar -->
   
  <!-- Jumbotron -->
  <section class="jumbotron" id="home">
    <main class="contain" data-aos="fade-right" data-aos-duration="1000">
      <h1 class="text-light">Nikmati Keindahan Alam dengan<br><span> Alat Camping</span> Terbaik</h1>
      <!-- <p>
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Placeat, numquam!
      </p> -->
      <a href="dashbord.php" class="btn btn-inti">Sewa Sekarang</a>
    </main>
  </section>
  <!-- End Jumbotron -->

  <!-- About -->
  <section class="about" id="about">
    <h2 data-aos="fade-down" data-aos-duration="1000">
      <span>Tentang</span> Kami
    </h2>
    <div class="row">
      <!-- <div class="about-img" data-aos="fade-right" data-aos-duration="1000">
        <img src="admin/img/bg.jpeg" alt="" />
      </div> -->
      <div class="contain" data-aos="fade-left" data-aos-duration="1000">
        <h4 class="text-center mb-3">Kenapa Memilih kami?</h4>
        <p>Star Adventure adalah pusat penyewaan alat camping yang menyediakan berbagai perlengkapan dan layanan untuk berbagai jenis kegiatan alam bebas. Tempat ini dirancang untuk memfasilitasi petualangan outdoor dan rekreasi bagi individu, kelompok, dan komunitas yang memiliki minat dalam menjelajahi alam. Star Adventure menawarkan beragam jenis perlengkapan camping yang dapat disewa untuk berbagai aktivitas, seperti mendaki gunung, berkemah, hiking, memancing, dan masih banyak lagi. Setiap peralatan dilengkapi dengan fitur yang sesuai, termasuk tenda berkualitas tinggi, sleeping bag nyaman, kompor portabel, peralatan masak, dan perlengkapan lain yang dibutuhkan untuk menjalankan aktivitas outdoor dengan lancar dan aman.</p>
      </div>
    </div>
  </section>
  <!-- End About -->

  <!-- Pembayaran -->
  <section class="pembayaran" id="bayar">
    <h2 data-aos="fade-down" data-aos-duration="1000">
      <span>Tata Cara</span> Penyewaan
    </h2>
    <p class="text-center">Berikut adalah tata cara penyewaan alat camping :</p>
    <ul class="border list-group list-group-flush mt-5">
      <li class="list-group-item">1. Pengguna masuk ke menu sewa sekarang.</li>
      <li class="list-group-item">2. Pengguna dapat melihat alat camping yang masih tersedia dan ingin disewa pada menu Alat Camping</li>
      <li class="list-group-item">3. Pada Menu Sewa Alat Camping Pengguna dapat melengkapi formulir penyewaan.</li>
      <li class="list-group-item">4. Lalu pengguna akan diarahkan ke form pembayaran</li>
      <li class="list-group-item">5. Lakukan pembayaran ke rekening yang sudah tertera dan upload bukti pembayaran</li>
      <li class="list-group-item">6. Bila Dirasa sudah sesuai, pengguna dapat meng klik tombol pesan.</li>
      <li class="list-group-item">7. Setelah terkirim, silahkan datang ke Star Adventure untuk mengambil barang yang disewa</li>
    </ul>
  </section>
  <!-- End Pembayaran -->

  <!-- Contact -->
  <section id="contact" class="contact" data-aos="fade-down" data-aos-duration="1000">
    <h2><span>Kontak</span> Kami</h2>
    <p class="text-center m-5">
      Hubungi kami jika ada saran yang ingin di sampaikan
    </p>
    <div class="row">
      <div class="col">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3952.7228724387737!2d110.39321457458378!3d-7.819130177649569!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a5713ed159c81%3A0xa0b05158e07df41d!2sStar%20Adventure%20Jogja%20Persewaan%20Alat%20Camping!5e0!3m2!1sid!2sid!4v1718341318682!5m2!1sid!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
      </div>
      <div class="col">
        <form action="">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i data-feather="user"></i></span>
            </div>
            <input type="text" name="" id="" placeholder="nama" class="form-control" />
          </div>
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i data-feather="mail"></i></span>
            </div>
            <input type="text" name="" id="" placeholder="email" class="form-control" />
          </div>
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i data-feather="phone"></i></span>
            </div>
            <input type="text" name="" id="" placeholder="no telp" class="form-control" />
          </div>
          <button type="submit" class="btn btn-inti mt-3">Kirim Pesan</button>
        </form>
      </div>
    </div>
  </section>
  <!-- End Contact -->

  <!-- footer -->
  <footer>
    <div class="social">
      <a href="#"><i data-feather="instagram"></i></a>
      <a href="#"><i data-feather="facebook"></i></a>
      <a href="#"><i data-feather="twitter"></i></a>
    </div>

    <div class="links">
      <a href="#home">Home</a>
      <a href="#about">Alat Camping</a>
      <a href="#menu">Pembayaran</a>
      <a href="#contact">Kontak</a>
    </div>

    <div class="credit">
      <p>Created by <a href="#">Era-Tech</a> &copy; 2024</p>
    </div>
  </footer>
  <!-- End Footer -->

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
  <script>
    feather.replace();
  </script>
</body>

</html>