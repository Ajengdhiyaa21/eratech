-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 12 Jul 2024 pada 05.36
-- Versi server: 10.4.24-MariaDB
-- Versi PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbalatcamping`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(60) NOT NULL,
  `password` varchar(80) NOT NULL,
  `nama_lengkap` varchar(60) NOT NULL,
  `nohp` varchar(16) NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_admin`
--

INSERT INTO `tb_admin` (`id_admin`, `username`, `password`, `nama_lengkap`, `nohp`, `level`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', '6283356703134', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_alat_camping`
--

CREATE TABLE `tb_alat_camping` (
  `id_alatCamping` int(11) NOT NULL,
  `nama_alatCamping` varchar(60) NOT NULL,
  `deskripsi` text NOT NULL,
  `jumlah_alatCamping` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `gambar_alatCamping` text NOT NULL,
  `jml_pinjam` int(11) NOT NULL,
  `jml_perbaiki` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_alat_camping`
--

INSERT INTO `tb_alat_camping` (`id_alatCamping`, `nama_alatCamping`, `deskripsi`, `jumlah_alatCamping`, `harga`, `gambar_alatCamping`, `jml_pinjam`, `jml_perbaiki`) VALUES
(1, 'Tenda Java 2 Pro', 'Dua Lapis, Kapasitas 2 Orang', 16, 35000, 'java 2 pro.jpg', 2, 0),
(2, 'Sleeping Bag', 'Sleeping Bag', 30, 8000, 'sleepingbag.jpg', 0, 0),
(3, 'Carrier 100L', 'Tas Gunung dengan kapasitas 100 Liter', 20, 25000, 'carrier100l.jpg', 0, 0),
(4, 'Carrier 60L', 'Tas Gunung dengan kapasitas 60 Liter', 10, 20000, 'carrier60l.jpg', 0, 0),
(5, 'Tenda Java 4 Pro', 'Double Layer, Kapasitas 4-5 Orang', 5, 55000, 'java4pro.jpeg', 0, 0),
(6, 'Tenda Dome Pavillo', 'Satu Layer, Kapasitas 2 Orang', 14, 25000, 'tendadome.jpeg', 0, 1),
(13, 'Tenda Big Dome', 'Double Layer, Kapasitas 7-8 orang', 5, 75000, 'bigdome.jpg', 0, 0),
(14, 'Tenda Rei Elliot', 'Double layer, Kapasitas 3-4 orang', 10, 40000, 'reielliot.jpeg', 0, 0),
(15, 'Kompor Mini Portable', 'Kompor Mini Portable', 5, 10000, 'kompor portabel.jpg', 0, 0),
(16, 'Kompor Koper', 'Kompor Koper', 5, 20000, 'kompor koper.jpeg', 0, 0),
(17, 'Cooking Set', 'Alat Memasak untuk digunung + ceret/teko (Tidak Sepaket Dengan Kompor)', 4, 12000, 'cookingset.webp', 1, 0),
(18, 'Gas ', 'Gas ini hanya untuk Kompor Portable (Kaleng Dikembalikan)', 12, 8000, 'gas.jpg', 0, 0),
(19, 'Sepatu', 'Sepatu khusus naik gunung, tersedia berbagai ukuran', 25, 25000, 'sepatu.jpg', 0, 0),
(20, 'Senter lentera', 'Senter lentera untuk penerangan + Baterai baru', 6, 12000, 'senter lentera.jpeg', 0, 0),
(21, 'Senter Kepala', 'Senter kepala digunakan untuk penerangan saat mendaki', 5, 12000, 'senterkepala.jpg', 0, 0),
(22, 'Tracking Pole', 'Tracking pole memberikan dukungan dan stabilitas selama kegiatan hiking, trekking, atau berjalan-jalan di alam terbuka.', 15, 12000, 'trackingpole.jpg', 0, 0),
(23, 'Fly sheet', 'Flysheet untuk melindungi tenda dari cuaca ekstrim. Fly sheet tersedia 2 ukuran (3x3 dan 3x4)', 5, 15000, 'flysheet.jpg', 0, 0),
(24, 'Tiang flysheet', 'Untuk menyangga flysheet', 5, 15000, 'tiang flysheet.jpg', 0, 0),
(25, 'Hammcok', 'Hammcok digunakan sebagai alat untuk beristirahat dengan melilitkannya di pohon atau tiang.', 15, 10000, 'hammock.png', 0, 0),
(26, 'Kursi Lipat', 'Kursi lipat dengan ukuran sedang', 10, 15000, 'kursi lipat.jpg', 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_denda`
--

CREATE TABLE `tb_denda` (
  `id_denda` int(11) NOT NULL,
  `harga_denda` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_denda`
--

INSERT INTO `tb_denda` (`id_denda`, `harga_denda`) VALUES
(1, 10000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_peminjam`
--

CREATE TABLE `tb_peminjam` (
  `id_peminjam` int(11) NOT NULL,
  `nama_peminjam` varchar(60) NOT NULL,
  `nohp` varchar(15) NOT NULL,
  `email` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_peminjam`
--

INSERT INTO `tb_peminjam` (`id_peminjam`, `nama_peminjam`, `nohp`, `email`) VALUES
(61, 'Arima Cipa', '62895123333452', 'aja@gmail.com'),
(62, 'Umah Usawah', '62877123456', 'usus@gmail.com'),
(63, 'Arima Cipa', '62895123333452', 'aja@gmail.com'),
(64, 'Ajeje', '6289124333156', 'aje@gmail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pengembalian`
--

CREATE TABLE `tb_pengembalian` (
  `id_kembali` int(11) NOT NULL,
  `idpinjam` int(11) NOT NULL,
  `tglkembali` date NOT NULL,
  `kondisi_alatCamping` varchar(60) NOT NULL,
  `catatan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_pengembalian`
--

INSERT INTO `tb_pengembalian` (`id_kembali`, `idpinjam`, `tglkembali`, `kondisi_alatCamping`, `catatan`) VALUES
(53, 55, '2024-07-13', '', ''),
(54, 56, '2024-07-12', 'Baik', 'Kondisi Baik - Lunas'),
(55, 57, '2024-07-13', '', ''),
(56, 58, '2024-07-12', 'Rusak', 'Perlu perbaikan - Denda Lunas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_sewa`
--

CREATE TABLE `tb_sewa` (
  `id_sewa` int(11) NOT NULL,
  `idalat` int(11) NOT NULL,
  `idpeminjam` int(11) NOT NULL,
  `jumlah_pinjam` int(11) NOT NULL,
  `harga_alat` int(11) NOT NULL,
  `harga_total_perhari` int(11) NOT NULL,
  `hari` int(11) NOT NULL,
  `tgLpinjam` date NOT NULL,
  `tgl_kembali` date NOT NULL,
  `harga_total_pinjam` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `foto` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_sewa`
--

INSERT INTO `tb_sewa` (`id_sewa`, `idalat`, `idpeminjam`, `jumlah_pinjam`, `harga_alat`, `harga_total_perhari`, `hari`, `tgLpinjam`, `tgl_kembali`, `harga_total_pinjam`, `status`, `foto`) VALUES
(55, 1, 61, 2, 35000, 35000, 1, '2024-07-12', '2024-07-13', 70000, 1, 0x363639306131386565303333302e6a706567),
(56, 3, 62, 2, 25000, 50000, 2, '2024-07-10', '2024-07-12', 100000, 2, 0x363639306131633263373666382e6a706567),
(57, 17, 63, 1, 12000, 12000, 1, '2024-07-12', '2024-07-13', 12000, 1, 0x363639306132353562373231332e6a706567),
(58, 6, 64, 1, 25000, 25000, 1, '2024-07-11', '2024-07-12', 25000, 2, 0x363639306132613338306432342e6a706567);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indeks untuk tabel `tb_alat_camping`
--
ALTER TABLE `tb_alat_camping`
  ADD PRIMARY KEY (`id_alatCamping`);

--
-- Indeks untuk tabel `tb_denda`
--
ALTER TABLE `tb_denda`
  ADD PRIMARY KEY (`id_denda`);

--
-- Indeks untuk tabel `tb_peminjam`
--
ALTER TABLE `tb_peminjam`
  ADD PRIMARY KEY (`id_peminjam`);

--
-- Indeks untuk tabel `tb_pengembalian`
--
ALTER TABLE `tb_pengembalian`
  ADD PRIMARY KEY (`id_kembali`);

--
-- Indeks untuk tabel `tb_sewa`
--
ALTER TABLE `tb_sewa`
  ADD PRIMARY KEY (`id_sewa`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tb_alat_camping`
--
ALTER TABLE `tb_alat_camping`
  MODIFY `id_alatCamping` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT untuk tabel `tb_denda`
--
ALTER TABLE `tb_denda`
  MODIFY `id_denda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_peminjam`
--
ALTER TABLE `tb_peminjam`
  MODIFY `id_peminjam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT untuk tabel `tb_pengembalian`
--
ALTER TABLE `tb_pengembalian`
  MODIFY `id_kembali` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT untuk tabel `tb_sewa`
--
ALTER TABLE `tb_sewa`
  MODIFY `id_sewa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
