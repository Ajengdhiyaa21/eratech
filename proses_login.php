<?php
// Include your database connection file here
include("admin/koneksi.php");

// Function to sanitize user input
function sanitizeInput($input) {
    return htmlspecialchars(strip_tags(trim($input)));
}

// Function to hash the password
function hashPassword($password) {
    return md5($password);
}

// Start the session
session_start();

// Check if the form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Get user input
    $username = sanitizeInput($_POST["username"]);
    $password = sanitizeInput($_POST["password"]);

    // Hash the password
    $hashedPassword = hashPassword($password);

    // Query to check user credentials
    $query = "SELECT * FROM tb_admin WHERE username = '$username' AND password = '$hashedPassword'";
    $result = mysqli_query($koneksi, $query);

    if ($result) {
        // Check if a row is returned
        if (mysqli_num_rows($result) == 1) {
            // Fetch user data
            $row = mysqli_fetch_assoc($result);

            // Set session variables
            $_SESSION['login_type'] = 'admin';
            $_SESSION["admin_id"] = $row["id_admin"];
            $_SESSION["username"] = $row["username"];
            $_SESSION["nama"] = $row["nama_lengkap"];
            $_SESSION["level"] = $row["level"];

            // Redirect to a logged-in page
            echo "<script>alert('Anda ".$_SESSION['nama']." Berhasil Login, Silakan Masuk.'); document.location='admin/index.php';</script>";
            exit();
        } else {
            echo "<script>alert('Invalid username or password.'); document.location='index.php';</script>";
        }
    } else {
        $error = "Error in database query: " . mysqli_error($koneksi);
    }
}
// Close the database koneksiection
mysqli_close($koneksi);
?>