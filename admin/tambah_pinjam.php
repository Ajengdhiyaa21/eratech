<?php 
session_start();
include 'koneksi.php';
if (!isset($_SESSION['login_type'])) {
    // Redirect to login page with SweetAlert2 alert
    echo "<script>alert('Anda Tidak Berhak Masuk, Login Terlebih Dahulu.'); document.location='../index.php';</script>";
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Alat Camping - Tambah Peminjaman</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

    <style>
        .custom-select {
            height: 300%; /* Adjust the width as needed */
        }
    </style>

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php include 'menu.php'; ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include 'navbar.php'; ?>
                
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Tambah Data</h1>

                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <a class="btn btn-danger" href="index.php"><i class="fas fa-arrow-left"></i> Kembali</a>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="proses_sewa.php" enctype="multipart/form-data">
                                <h4 align="center">Data Peminjam:</h4>
                                <hr>
                                <div class="form-group">
                                    <label>Nama Peminjam</label>
                                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama Peminjam">
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>No. Hp</label>
                                            <input type="number" class="form-control" id="nohp" name="nohp" placeholder="Masukkan No. Hp">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan Email">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <hr>

                                <h4 align="center">Data Alat Camping:</h4>
                                <br>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Pilih ALat Camping</label>
                                            <select class="form-control" id="alat" name="alat">
                                                <option selected disabled>Pilih Alat Camping</option>
                                                <?php 
                                                $dataA = mysqli_query($koneksi, "SELECT * FROM tb_alat_Camping");
                                                while ($ra = mysqli_fetch_assoc($dataA)){
                                                ?>
                                                <option value="<?php echo $ra['id_alatCamping']; ?>"><?php echo $ra['nama_alatCamping']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Harga alat perhari</label>
                                            <input type="text" class="form-control" id="harga1" readonly>
                                            <input type="hidden" id="harga" name="harga">
                                        </div>

                                        <div class="form-group">
                                            <label>Tanggal Pinjam</label>
                                            <input type="date" class="form-control" id="tgl_pinjam" name="tgl_pinjam" onchange="hitungSelisihHari()">
                                        </div>

                                        <div class="form-group">
                                            <label>Tanggal Kembali</label>
                                            <input type="date" class="form-control" id="tgl_kembali" name="tgl_kembali" onchange="hitungSelisihHari()">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Lama Hari Pinjam</label>
                                            <input type="text" class="form-control" id="jumlah_hari" name="jumlah_hari" readonly>
                                        </div>

                                        <div class="form-group">
                                            <label>Harga Total Pinjam Perhari</label>
                                            <input type="text" class="form-control" id="total_h" readonly>
                                            <input type="hidden" id="total_harga" name="total_harga">
                                        </div>

                                        <div class="form-group">
                                            <label>Jumlah Pinjam Alat Camping</label>
                                            <input type="text" class="form-control" id="jumlah_pinjam" name="jumlah_pinjam">
                                        </div>

                                        <div class="form-group">
                                            <label>Total Semua Harga</label>
                                            <input type="text" class="form-control" id="total" readonly>
                                            <input type="hidden" id="total1" name="total1">
                                        </div>
                                    </div>
                                </div>
                                    
                                    <br>
                                    <hr>
                                    <h4 style="text-align: center;">Pembayaran</h4>

                                    <br>
                                    <div class="col-md-6"></div>
                                    <div class="row">
                                    <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="exampleInputPassword1" class="form-label"></label>
                                    <label for="exampleInputPassword1" class="form-label">Transfer ke : </label>
                                    <label for="exampleInputPassword1" class="form-label">BRI 0892322132 a/n Star Adventure</label>
                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1" class="form-label">Upload Bukti</label>
                                            <input type="file" name="foto" class="form-control" id="exampleInputPassword1">
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                                </div>

                                <div class="modal-footer">
                                    <button class="btn btn-primary" type="submit">Simpan</button>

                                    <button class="btn btn-warning" type="reset">Reset</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                    <span>Copyright &copy; Era-Tech 2024</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="keluar.php">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>

    <script>
    // Inisialisasi Select2 pada elemen dengan class "select2"
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>

<script>
    function number_format(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    // Mendapatkan elemen select
    var alatSelect = document.getElementById('alat');

    // Menambahkan event listener untuk merespons perubahan pada select
    alatSelect.addEventListener('change', function () {
        // Mendapatkan nilai yang dipilih
        var selectedAlatId = this.value;

        // Mengirim request AJAX ke server untuk mendapatkan harga alat Camping berdasarkan ID
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                // Mendapatkan data JSON dari server
                var responseData = JSON.parse(xhr.responseText);

                // Menampilkan harga di form input
                document.getElementById('harga').value = responseData.harga;

                document.getElementById('harga1').value = "Rp. " + number_format(responseData.harga) + ",-";
            }
        };

        // Mengirim request GET ke server
        xhr.open('GET', 'get_harga.php?id=' + selectedAlatId, true);
        xhr.send();
    });
</script>

<script>
    function number_format(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function hitungSelisihHari() {
        var tglPinjam = new Date(document.getElementById("tgl_pinjam").value);
        var tglKembali = new Date(document.getElementById("tgl_kembali").value);
        var satuHari = 24 * 60 * 60 * 1000; // Satu hari dalam milidetik

        var selisihHari = Math.round(Math.abs((tglPinjam - tglKembali) / satuHari));
        document.getElementById("jumlah_hari").value = selisihHari;

        var hargaPerHari = parseFloat(document.getElementById("harga").value);
        if (!isNaN(hargaPerHari)) {
            var totalHarga = hargaPerHari * selisihHari;

            // Get the value of jumlah_pinjam
            var jumlahPinjam = parseInt(document.getElementById("jumlah_pinjam").value) || 0;

            // Multiply totalHarga with jumlahPinjam
            var totalSemuaHarga = totalHarga * jumlahPinjam;

            document.getElementById("total_harga").value = totalHarga;
            document.getElementById("total_h").value = "Rp. " + number_format(totalHarga) + ",-";

            document.getElementById("total").value = "Rp. " + number_format(totalSemuaHarga) + ",-";
            document.getElementById("total1").value = totalSemuaHarga;
        }
    }

    document.getElementById("alat").addEventListener("change", function() {
        var selectedAlatId = this.value;
        var selectedAlat = <?php echo json_encode($ra); ?>; // Assuming $ra is the array containing the selected alat Camping's data

        if (selectedAlatId === "") {
            document.getElementById("harga").value = "";
        } else {
            var harga = selectedAlat[selectedAlatId].harga_alatCamping;
            document.getElementById("harga").value = harga;
        }

        hitungSelisihHari();
    });

    // Add an event listener for the "input" event on the jumlah_pinjam input field
    document.getElementById("jumlah_pinjam").addEventListener("input", function() {
        hitungSelisihHari();
    });
</script>

<script>
    document.getElementById('nohp').addEventListener('input', function () {
        var inputNoHp = document.getElementById('nohp').value;

        // Menghapus karakter non-angka dari nomor HP
        var cleanNoHp = inputNoHp.replace(/\D/g, '');

        // Jika nomor HP dimulai dengan "0", gantilah dengan "62"
        if (cleanNoHp.startsWith('0')) {
            cleanNoHp = '62' + cleanNoHp.substring(1);
        }

        // Update nilai input dengan nomor HP yang telah diubah
        document.getElementById('nohp').value = cleanNoHp;
    });
</script>



</body>

</html>