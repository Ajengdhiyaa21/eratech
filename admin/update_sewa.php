<?php
// Koneksi ke database
include 'koneksi.php';

// Mendapatkan data untuk update dari formulir atau sumber lainnya
$id_sewa = $_POST['ids'];
$id_alat = $_POST['id_alat'];
$jumlah_alatCamping = $_POST['jumlah'];
$status_sewa = $_POST['status'];
$kondisi_alatCamping = $_POST['statusAlatCamping'];
$catatan = $_POST['catatan'];

// Update tabel tb_sewa
$sql_tb_sewa = "UPDATE tb_sewa SET status = '$status_sewa' WHERE id_sewa = '$id_sewa'";
$result_tb_sewa = $koneksi->query($sql_tb_sewa);

if ($kondisi_alatCamping == 'Baik') {
    // Update tabel tb_alat_Camping
    $sql_tb_alat_Camping = "UPDATE tb_alat_Camping SET jumlah_alatCamping = jumlah_alatCamping + $jumlah_alatCamping, jml_pinjam = jml_pinjam - $jumlah_alatCamping WHERE id_alatCamping = '$id_alat'";
    $result_tb_alat_Camping = $koneksi->query($sql_tb_alat_Camping);
}elseif ($kondisi_alatCamping =='Rusak') {
    // Update tabel tb_alat_Camping
    $sql_tb_alat_Camping = "UPDATE tb_alat_Camping SET jml_pinjam = jml_pinjam - $jumlah_alatCamping, jml_perbaiki = jml_perbaiki + $jumlah_alatCamping WHERE id_alatCamping = '$id_alat'";
    $result_tb_alat_Camping = $koneksi->query($sql_tb_alat_Camping);
}

// Update tabel tb_pengembalian
$sql_tb_pengembalian = "UPDATE tb_pengembalian SET kondisi_alatCamping = '$kondisi_alatCamping', catatan = '$catatan' WHERE idpinjam = '$id_sewa'";
$result_tb_pengembalian = $koneksi->query($sql_tb_pengembalian);

// Periksa apakah update berhasil
if ($result_tb_sewa && $result_tb_alat_Camping && $result_tb_pengembalian) {
    echo "<script>alert('Data berhasil diperbarui.'); document.location='sewa.php';</script>";
} else {
    echo "Terjadi kesalahan saat memperbarui data: " . $koneksi->error;
}

// Tutup koneksi ke database
$koneksi->close();
?>