        <?php if ($_SESSION['level'] == 'admin') { ?>
            <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

                <!-- Sidebar - Brand -->
                <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
                    <div class="sidebar-brand-icon rotate-n-15">
                        <!-- <i class="fas fa-truck"></i> -->
                    </div>
                    <div class="sidebar-brand-text mx-3">Alat <sup>Camping</sup></div>
                </a>

                <!-- Divider -->
                <hr class="sidebar-divider my-0">

                <!-- Nav Item - Dashboard -->
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span></a>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider">

                <!-- Heading -->
                <div class="sidebar-heading">
                    Alat Camping
                </div>

                <!-- Nav Item - Pages Collapse Menu -->
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                        aria-expanded="true" aria-controls="collapseTwo">
                        <i class="fas fa-fw fa-cog"></i>
                        <span>Master</span>
                    </a>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <h6 class="collapse-header">Data</h6>
                            <a class="collapse-item" href="alat_Camping.php">Alat Camping</a>
                            <a class="collapse-item" href="peminjam.php">Peminjam</a>
                        </div>
                    </div>
                </li>

                <!-- Nav Item - Utilities Collapse Menu -->
                <li class="nav-item">
                    <a class="nav-link collapsed" href="sewa.php">
                        <i class="fas fa-fw fa-book"></i>
                        <span>Sewa Alat Camping</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link collapsed" href="pengembalian.php">
                        <i class="fas fa-fw fa-arrow-left"></i>
                        <span>Pengembalian</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link collapsed" href="denda.php">
                        <i class="fas fa-fw fa-dollar-sign"></i>
                        <span>Denda</span>
                    </a>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider">

                <!-- Heading -->
                <div class="sidebar-heading">
                    Lainnya
                </div>

                <!-- Nav Item - pengguna -->
                <li class="nav-item">
                    <a class="nav-link" href="pengguna.php">
                        <i class="fas fa-fw fa-user"></i>
                        <span>Pengguna</span></a>
                </li>

                <!-- Nav Item - Laporan Collapse Menu -->
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
                        aria-expanded="true" aria-controls="collapsePages">
                        <i class="fas fa-fw fa-folder"></i>
                        <span>Laporan</span>
                    </a>
                    <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <h6 class="collapse-header">Laporan</h6>
                            <a class="collapse-item" href="lap_sewa.php">Lap. Sewa Alat Camping</a>
                        </div>
                    </div>
                </li>

                <!-- Nav Item - keluae -->
                <li class="nav-item">
                    <a class="nav-link" href="keluar.php">
                        <i class="fas fa-fw fa-sign-out-alt"></i>
                        <span>Keluar</span></a>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider d-none d-md-block">

                <!-- Sidebar Toggler (Sidebar) -->
                <div class="text-center d-none d-md-inline">
                    <button class="rounded-circle border-0" id="sidebarToggle"></button>
                </div>
            </ul>
        <?php }elseif ($_SESSION['level'] == 'user') { ?>
            <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

                <!-- Sidebar - Brand -->
                <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
                    <div class="sidebar-brand-icon rotate-n-15">
                        <!-- <i class="fas fa-truck"></i> -->
                    </div>
                    <div class="sidebar-brand-text mx-3">Alat <sup>Camping</sup></div>
                </a>

                <!-- Divider -->
                <hr class="sidebar-divider my-0">

                <!-- Nav Item - Dashboard -->
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span></a>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider">

                <!-- Heading -->
                <div class="sidebar-heading">
                    Alat Camping
                </div>

                <!-- Nav Item - Pages Collapse Menu -->

                <!-- Nav Item - Utilities Collapse Menu -->
                <li class="nav-item">
                <a class="nav-link collapsed" href="tambah_pinjam.php">
                        <i class="fas fa-fw fa-book"></i>
                        <span>Sewa Alat Camping</span>
                    </a>
                    <a class="nav-link collapsed" href="menualat.php">
                        <i class="fas fa-fw fa-book"></i>
                        <span>Alat Camping</span>
                    </a>
                </li>

                <!-- Nav Item - keluae -->
                <li class="nav-item">
                    <a class="nav-link" href="keluar.php">
                        <i class="fas fa-fw fa-sign-out-alt"></i>
                        <span>Keluar</span></a>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider d-none d-md-block">

                <!-- Sidebar Toggler (Sidebar) -->
                <div class="text-center d-none d-md-inline">
                    <button class="rounded-circle border-0" id="sidebarToggle"></button>
                </div>
            </ul>
        <?php } ?>