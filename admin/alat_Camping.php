<?php 
session_start();
include 'koneksi.php';
if (!isset($_SESSION['login_type'])) {
    // Redirect to login page with SweetAlert2 alert
    echo "<script>alert('Anda Tidak Berhak Masuk, Login Terlebih Dahulu.'); document.location='../index.php';</script>";
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sewa Alat Camping - Tabel Alat Camping</title>

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

    <!-- Custom styles for this page -->
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php include 'menu.php'; ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include 'navbar.php'; ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Tabel Alat Camping</h1>
                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <a class="btn btn-primary" href="tambah_alat.php"><i class="fas fa-plus"></i> Tambah Data</a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama Alat Camping</th>
                                            <th>Deskripsi</th>
                                            <th>Jumlah</th>
                                            <th>Harga</th>
                                            <th>Gambar</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $no = 1;

                                        $data = mysqli_query($koneksi, "SELECT * FROM tb_alat_Camping");
                                        while ($row = mysqli_fetch_assoc($data)){

                                            $id_alatCamping = $row['id_alatCamping'];
                                            $jumlah_alatCamping = $row['jumlah_alatCamping'];
                                            $jumlah_dipinjam = $row['jml_pinjam'];
                                            $jumlah_diperbaiki = $row['jml_perbaiki'];

                                            $status = '<span class="badge badge-primary">Tersedia Semua</span>';

                                            if ($jumlah_dipinjam > 0 && $jumlah_diperbaiki > 0) {
                                                $status = '<span class="badge badge-warning">Disewa '.$jumlah_dipinjam.' masih disewa dan Diperbaiki '. $jumlah_diperbaiki. ' masih diperbaiki. Tersedia ' . ($jumlah_alatCamping) . '</span>';
                                            } elseif ($jumlah_dipinjam > 0) {
                                                if ($jumlah_dipinjam > $jumlah_alatCamping) {
                                                    $status = '<span class="badge badge-success">Disewa semua</span>';
                                                } else {
                                                    $status = '<span class="badge badge-info">Disewa '.$jumlah_dipinjam.' Tinggal ' . ($jumlah_alatCamping) . ' Tersedia</span>';
                                                }
                                            } elseif ($jumlah_diperbaiki > 0) {
                                                if ($jumlah_diperbaiki > $jumlah_alatCamping) {
                                                    $status = '<span class="badge badge-danger">Diperbaiki semua</span>';
                                                } else {
                                                    $status = '<span class="badge badge-secondary">Diperbaiki '. $jumlah_diperbaiki. ' Dengan tersedia ' . ($jumlah_alatCamping) . ' lagi</span>';
                                                }
                                            }
                                        ?>
                                        <tr>
                                            <td><?php echo $no++; ?></td>
                                            <td><?php echo $row['nama_alatCamping']; ?></td>
                                            <td align="justify"><?php echo $row['deskripsi']; ?></td>
                                            <td><?php echo $row['jumlah_alatCamping']; ?></td>
                                            <td><?php echo "Rp. " .number_format($row['harga']). ",-"; ?></td>
                                            <td align="center">
                                                <img src="alat_Camping/<?php echo $row['gambar_alatCamping']; ?>" width="100px" height="100px" title="<?php echo $row['nama_alatCamping']; ?>">
                                            </td>
                                            <td align="center"><?php echo $status; ?></td>
                                            <td>
                                                <a class="btn btn-sm btn-warning" href="edit_alat.php?id=<?php echo $row['id_alatCamping']; ?>"><i class="fas fa-edit"></i></a>

                                                <a class="btn btn-sm btn-danger" href="hapus_alat.php?id=<?php echo $row['id_alatCamping']; ?>"><i class="fas fa-trash"></i></a>
                                                <?php if ($jumlah_diperbaiki != 0) { ?>
                                                    <a class="btn btn-sm btn-primary" href="" data-toggle="modal" data-target="#myModal<?php echo $row['id_alatCamping']; ?>"><i class="fas fa-clock"></i></a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

                <!-- Modal -->
                <?php 
                $a = mysqli_query($koneksi, "SELECT * FROM tb_alat_Camping");
                while ($ra = mysqli_fetch_assoc($a)){
                ?>
                <div class="modal fade" id="myModal<?php echo $ra['id_alatCamping']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Status Perbaikan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <!-- Formulir -->
                        <form method="POST" action="update_status_alat.php">
                            <input type="hidden" name="ida" value="<?php echo $ra['id_alatCamping']; ?>">
                            <input type="hidden" name="jmla" value="<?php echo $ra['jumlah_alatCamping']; ?>">
                            <input type="hidden" name="jmlp" value="<?php echo $ra['jml_perbaiki']; ?>">
                          <div class="form-group">
                            <label for="statusSelect">Pilih Status:</label>
                            <select class="form-control" id="statusSelect" name="status">
                              <option selected disabled>Pilih Status</option>
                              <option value="Selesai">Selesai Diperbaiki</option>
                            </select>
                          </div>
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <?php } ?>

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                    <span>Copyright &copy; Era-Tech 2024</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="keluar.php">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/datatables-demo.js"></script>

</body>

</html>