<?php
// Koneksi ke database
include 'koneksi.php';

// ID record yang akan diupdate
$id_denda = $_POST['id_denda']; // Gantilah dengan ID yang sesuai

// Harga denda baru
$harga_denda_baru = $_POST['hargaDenda']; // Gantilah dengan harga denda yang baru

// Query SQL untuk update
$sql = "UPDATE tb_denda SET harga_denda = $harga_denda_baru WHERE id_denda = $id_denda";

// Eksekusi query
if ($koneksi->query($sql) === TRUE) {
    echo "<script>alert('Data berhasil diperbarui.'); document.location='denda.php';</script>";
} else {
    echo "Error: " . $sql . "<br>" . $koneksi->error;
}

// Tutup koneksi
$koneksi->close();
?>