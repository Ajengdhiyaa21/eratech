<?php 
session_start();
include 'koneksi.php';
if (!isset($_SESSION['login_type'])) {
    // Redirect to login page with SweetAlert2 alert
    echo "<script>alert('Anda Tidak Berhak Masuk, Login Terlebih Dahulu.'); document.location='../index.php';</script>";
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Alat Camping - Tambah Alat Camping</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php include 'menu.php'; ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include 'navbar.php'; ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Edit Data</h1>

                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <a class="btn btn-danger" href="alat_Camping.php"><i class="fas fa-arrow-left"></i> Kembali</a>
                        </div>
                        <?php 
                        $id = $_GET['id'];

                        $data = mysqli_query($koneksi, "SELECT * FROM tb_alat_Camping WHERE id_alatCamping = $id");
                        $row = mysqli_fetch_assoc($data);
                        ?>
                        <div class="card-body">
                            <form method="POST" action="update_alat.php" enctype="multipart/form-data">
                                <input type="hidden" name="id_alatCamping" value="<?php echo $id; ?>">
                                <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                           <label>Nama Alat Camping</label>
                                           <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama Alat Camping" value="<?php echo $row['nama_alatCamping']; ?>">
                                       </div>

                                       <div class="form-group">
                                           <label>Deskripsi</label>
                                           <textarea class="form-control" id="deskripsi" name="deskripsi" rows="4" placeholder="Masukkan Deskripsi Alat Camping"><?php echo $row['deskripsi']; ?></textarea>
                                       </div> 
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Jumlah Alat Camping</label>
                                            <input type="number" class="form-control" id="jumlah" name="jumlah" placeholder="Masukkan Jumlah Alat Camping" value="<?php echo $row['jumlah_alatCamping']; ?>">
                                        </div>

                                        <div class="form-group">
                                            <label>Harga Sewa</label>
                                            <input type="text" class="form-control" id="harga" name="harga" placeholder="Masukkan Harga Sewa" value="<?php echo $row['harga']; ?>">
                                        </div>

                                        <div class="form-group">
                                            <img src="alatCamping/<?php echo $row['gambar_alatCamping']; ?>" width="120px" height="120px" title="<?php echo $row['nama_alatCamping']; ?>"><br><br>
                                            <label>Ubah Gambar Alat Camping</label>
                                            <input type="file" class="form-control" id="filealat" name="filealat">
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button class="btn btn-primary" type="submit">Simpan</button>

                                    <button class="btn btn-warning" type="reset">Reset</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                    <span>Copyright &copy; Era-Tech 2024</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="keluar.php">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

</body>

</html>