<?php
// Assuming you have established a database connection
include 'koneksi.php';

// Get the values from the form or wherever you have them
$nama_peminjam = $_POST['nama'];
$nohp = $_POST['nohp'];
$email = $_POST['email'];
$id_alat = $_POST['alat'];
$harga_alat = $_POST['harga'];
$tgl_pinjam = $_POST['tgl_pinjam'];
$tgl_kembali = $_POST['tgl_kembali'];
$hari = $_POST['jumlah_hari'];
$harga_t_p = $_POST['total_harga'];
$jumlah_pinjam = $_POST['jumlah_pinjam'];
$harga_s_t = $_POST['total1'];

// Insert data into tb_peminjam
$sql_peminjam = "INSERT INTO tb_peminjam (nama_peminjam, nohp, email) VALUES ('$nama_peminjam', '$nohp', '$email')";

if ($koneksi->query($sql_peminjam) === TRUE) {
    $id_peminjam = $koneksi->insert_id; // Get the ID of the inserted record
    // Insert data into tb_peminjaman
    $sql_sewa = "INSERT INTO tb_sewa (idalat, idpeminjam, jumlah_pinjam, harga_alat, harga_total_perhari, hari, tgLpinjam, tgl_kembali, harga_total_pinjam, status) VALUES ('$id_alat', '$id_peminjam', '$jumlah_pinjam', '$harga_alat', '$harga_t_p', '$hari', '$tgl_pinjam', '$tgl_kembali', '$harga_s_t', '1')";

    if ($koneksi->query($sql_sewa) === TRUE) {
        $id_sewa = $koneksi->insert_id; // Get the ID of the inserted record
        // Insert data into tb_pengembalian
        $sql_pengembalian = "INSERT INTO tb_pengembalian (idpinjam, tglkembali) VALUES ('$id_sewa', '$tgl_kembali')";

        if ($koneksi->query($sql_pengembalian) === TRUE) {
            // Update tb_alat_Camping table
            $sql_update_alat = "UPDATE tb_alat_Camping SET jumlah_alatCamping = jumlah_alatCamping - $jumlah_pinjam, jml_pinjam = jml_pinjam + $jumlah_pinjam WHERE id_alatCamping = '$id_alat'";
            
            if ($koneksi->query($sql_update_alat) === TRUE) {
                echo "<script>alert('Data berhasil disimpan.'); document.location='tambah_pinjam.php';</script>";
            } else {
                echo "Error updating tb_alat_Camping: " . $koneksi->error;
            }
        } else {
            echo "Error inserting into tb_pengembalian: " . $koneksi->error;
        }
    } else {
        echo "Error inserting into tb_peminjaman: " . $koneksi->error;
    }
} else {
    echo "Error inserting into tb_peminjam: " . $koneksi->error;
}

// Close the connection
$koneksi->close();
?>