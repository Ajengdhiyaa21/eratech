<?php
// Include your database connection file here
include('koneksi.php');

// Get values from the form
$id_admin = $_POST['id_admin']; // Assuming you have the admin ID to update
$username = $_POST['username'];
$nama_lengkap = $_POST['nama'];
$nohp = $_POST['nohp'];
$level = $_POST['level'];

// Check if password is provided
if (!empty($_POST['password'])) {
    // If password is provided, update it in the database
    $password = md5($_POST['password']); // Use a more secure hashing algorithm in production

    // SQL query to update data in tb_admin with password
    $sql = "UPDATE tb_admin 
            SET username='$username', password='$password', 
                nama_lengkap='$nama_lengkap', nohp='$nohp', level='$level' 
            WHERE id_admin='$id_admin'";
} else {
    // If password is not provided, update other fields without changing the password in the database

    // SQL query to update data in tb_admin without password
    $sql = "UPDATE tb_admin 
            SET username='$username', 
                nama_lengkap='$nama_lengkap', nohp='$nohp', level='$level' 
            WHERE id_admin='$id_admin'";
}

// Execute the query
if ($koneksi->query($sql) === TRUE) {
    echo "<script>alert('Data berhasil diperbarui.'); document.location='pengguna.php';</script>";
} else {
    echo "Error: " . $sql . "<br>" . $koneksi->error;
}

// Close the database connection
$koneksi->close();
?>