<?php
// Mengambil data alat Camping yang disewa dan memiliki status 'sewa'
$sql = "SELECT tb_alat_Camping.nama_alatCamping, tb_sewa.tgLpinjam, tb_sewa.tgl_kembali
        FROM tb_sewa
        INNER JOIN tb_alat_Camping ON tb_sewa.idalat = tb_alat_Camping.id_alatCamping
        WHERE tb_sewa.status = '1'";
$result = $koneksi->query($sql);

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $nama_alatCamping = $row["nama_alatCamping"];
        $tgl_sewa = $row["tgLpinjam"];
        $tgl_kembali = $row["tgl_kembali"];

        // Menghitung selisih hari antara tanggal sekarang dan tanggal kembali
        $today = date("Y-m-d");
        $selisih_hari = (strtotime($today) - strtotime($tgl_kembali)) / (60 * 60 * 24);

        $selisih = strtotime($tgl_kembali) - strtotime($tgl_sewa);
        $selisih = floor($selisih / (60 * 60 * 24));

        // Menampilkan notifikasi berdasarkan kondisi
        if ($selisih_hari > 0) {
            echo "
                <a class='dropdown-item d-flex align-items-center' href='#'>
                    <div class='mr-3'>
                        <div class='icon-circle bg-danger'>
                            <i class='fas fa-exclamation-triangle text-white'></i>
                        </div>
                    </div>
                    <div>
                        <div class='small text-gray-500'>$tgl_kembali</div>
                        <span class='font-weight-bold'>Alat Camping $nama_alatCamping disewa selama $selisih hari dan sudah telat dalam pengembalian.</span>
                    </div>
                </a>";
        } elseif ($selisih_hari < 1) {
            echo "
                <a class='dropdown-item d-flex align-items-center' href='#'>
                    <div class='mr-3'>
                        <div class='icon-circle bg-success'>
                            <i class='fas fa-check text-white'></i>
                        </div>
                    </div>
                    <div>
                        <div class='small text-gray-500'>$tgl_kembali</div>
                        <span class='font-weight-bold'>Alat Camping $nama_alatCamping disewa selama $selisih hari dan sudah dikembalikan.</span>
                    </div>
                </a>";
        } else {
            echo "
                <a class='dropdown-item d-flex align-items-center' href='#'>
                    <div class='mr-3'>
                        <div class='icon-circle bg-primary'>
                            <i class='fas fa-file-alt text-white'></i>
                        </div>
                    </div>
                    <div>
                        <div class='small text-gray-500'>$tgl_sewa</div>
                        <span class='font-weight-bold'>Alat Camping $nama_alatCamping disewa selama $selisih hari dan masih dalam masa sewa.</span>
                    </div>
                </a>";
        }
    }
} else {
    echo "Tidak ada alat Camping yang sedang disewa.";
}
?>