<?php
// Database connection parameters
include 'koneksi.php';

// Check if the form is submitted
if(isset($_POST['id'])) {
    // Escape user inputs for security
    $id_alat = $koneksi->real_escape_string($_POST['id']);

    // SQL query to delete a row from the table
    $sql = "DELETE FROM tb_alat_Camping WHERE id_alatCamping = $id_alat";

    // Execute the query
    if ($koneksi->query($sql) === TRUE) {
        echo "<script>alert('Data berhasil dihapus.'); document.location='alat_Camping.php';</script>";
    } else {
        echo "Error deleting record: " . $koneksi->error;
    }
}

// Close the database koneksiection
$koneksi->close();
?>