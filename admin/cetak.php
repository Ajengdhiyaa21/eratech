<?php
include 'koneksi.php';
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Alat Camping - Laporan</title>

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

    <!-- Custom styles for this page -->
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

    <style>
		  @media print {
		    /* Reset some default styles for printing */
		    .row {
		      display: flex;
		      justify-content: space-between;
		    }

		    .col-md-6 {
		      width: 48%; /* Adjust as needed to leave some space between the two columns */
		    }
		  }

		  img {
		    width: 130%;
		    height: auto;
		    display: block;
		    margin: 0 auto;
		  }

		  h4, h6 {
		    margin: 5px 0;
		  }
		</style>

</head>
<body class="container mt-5">

	<table style="width: 100%; border-collapse: collapse;">
	  <tr style="font-family: 'Times New Roman', Times, serif;">
	    <td width="25" style="text-align: center; vertical-align: middle; padding: 10px;"><img src="img/logo.jpeg"></td>
	    <td width="50" style="text-align: center; vertical-align: middle; padding: 10px;">
	      <h4><b>PENYEWAAN ALAT CAMPING STAR ADVENTURE</b></h4>
	      <h4><b>KECAMATAN KOTAGEDE</b></h4>
	      <h6><b>Jl. Ngeksigondo No.3, Prenggan, Kec. Kotagede, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55172</b></h6>
	      <h6><b>No. Telp: 0897-6797-162</b></h6>
	    </td>
	    <td width="25" style="text-align: center; vertical-align: middle; padding: 10px;"><img src="img/logo.jpeg"></td>
	  </tr>
	</table>
	<hr style="border: 2px solid black;">
	<br>
	<h4 align="center" style="font-family: 'Times New Roman', Times, serif;"><b>Formulir Sewa Alat Camping</b></h4><br>

	<table class="table table-bordered" width="100%" cellspacing="0">
		<thead>
			<tr>
				<th>No.</th>
				<th>Nama Penyewa</th>
				<th>Alat Camping</th>
				<th>Tgl Sewa</th>
				<th>Tgl Kembali</th>
				<th>status</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$no =1;

			$id = $_GET['id'];
			$tglSewa = $_GET['sewa'];
      $tglKembali = $_GET['kembali'];
      $status = $_GET['status'];

			$data = mysqli_query($koneksi, "SELECT * FROM tb_sewa, tb_peminjam, tb_alat_Camping WHERE idpeminjam = id_peminjam AND idalat = id_alatCamping AND tgLpinjam >= '$$tglSewa' AND tgl_kembali <= '$tglKembali' AND status = '$status' AND id_sewa = '$id'");
			while ($row = mysqli_fetch_assoc($data)){
			?>
			<tr>
				<td><?php echo $no++; ?>.</td>
				<td><?php echo $row['nama_peminjam']; ?></td>
				<td><?php echo $row['nama_alatCamping']; ?></td>
				<td><?php echo $row['tgLpinjam']; ?></td>
				<td><?php echo $row['tgl_kembali']; ?></td>
				<td>
					<?php if ($row['status'] == 1) { ?>
            <span class="badge badge-primary">Sewa</span>
          <?php }elseif ($row['status'] == 2) { ?>
            <span class="badge badge-success">Kembali</span>
          <?php } ?>
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>

	<div class="row">
    <div class="col-md-6">
      <table align="left" style="width: 37%; border-collapse: collapse;">
        <tr>
          <td align="center" style="border: 0px solid #ddd; padding: 8px;">
           	<br />
            Nama Penyewa,<br/>
            <br /><br>
            <br><b><u>(.....................)</u></b>
          </td>
        </tr>
      </table>
    </div>

   	<div class="col-md-6">
      <table align="right" style="width: 49%; border-collapse: collapse;">
        <tr>
          <td align="center" style="border: 0px solid #ddd; padding: 8px;">
            Star Adventure, <?php echo date('d F Y'); ?><br />
            Pemilik,<br/>
            <br /><br>
            <br><b><u>(.....................)</u></b>
          </td>
        </tr>
      </table>
    </div>
  </div>
	

<br><br><br>
		<!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/datatables-demo.js"></script>

    <script>
    	window.print();
    </script>
</body>
</html>