<?php
// Koneksi ke database
include 'koneksi.php';

// Tangkap data dari formulir
$id_alatCamping = $_POST['id_alatCamping']; // Diambil dari formulir atau parameter URL
$nama_alatCamping = $_POST['nama'];
$deskripsi = $_POST['deskripsi'];
$jumlah_alatCamping = $_POST['jumlah'];
$harga = $_POST['harga'];

// Periksa apakah gambar baru diunggah
if ($_FILES['filealat']['size'] > 0) {
    // Upload gambar baru
    $gambar = $_FILES['filealat']['name'];
    $gambar_tmp = $_FILES['filealat']['tmp_name'];
    $upload_dir = "alatCamping/"; // Sesuaikan dengan direktori tempat gambar diunggah

    // Pindahkan gambar ke direktori tujuan
    move_uploaded_file($gambar_tmp, $upload_dir . $gambar);

    // Update data dengan gambar baru
    $sql = "UPDATE tb_alat_Camping 
            SET nama_alatCamping='$nama_alatCamping', deskripsi='$deskripsi', jumlah_alatCamping='$jumlah_alatCamping', harga='$harga', gambar_alatCamping='$gambar'
            WHERE id_alatCamping=$id_alatCamping";
} else {
    // Update data tanpa mengubah gambar
    $sql = "UPDATE tb_alat_Camping 
            SET nama_alatCamping='$nama_alatCamping', deskripsi='$deskripsi', jumlah_alatCamping='$jumlah_alatCamping', harga='$harga'
            WHERE id_alatCamping=$id_alatCamping";
}

if ($koneksi->query($sql) === TRUE) {
    echo "<script>alert('Data berhasil diperbarui.'); document.location='alat_Camping.php';</script>";
} else {
    echo "Error: " . $sql . "<br>" . $koneksi->error;
}

// Tutup koneksi
$koneksi->close();
?>