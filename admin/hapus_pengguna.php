<?php
include 'koneksi.php';

// Get the id_admin from the request or any other source
$id_admin = $_GET['id'];

// SQL to delete a record
$sql = "DELETE FROM tb_admin WHERE id_admin = $id_admin";

if ($koneksi->query($sql) === TRUE) {
    echo "<script>alert('Data berhasil dihapus.'); document.location='pengguna.php';</script>";
} else {
    echo "Error deleting record: " . $koneksi->error;
}

// Close the koneksiection
$koneksi->close();
?>